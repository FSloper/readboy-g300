# 读书郎 readboy G300开发记录

## 1.刷机包
为了解决防止变砖和已经变砖,当然这个需要刷机包,我在万千网络中终于在淘宝找到了量产包,50块大洋真的离谱,当然,我这里会分享给大家的,
[百度网盘链接](https://pan.baidu.com/s/1XHcKFG1F-2IOROkpv8Pcyg?pwd=1111) ,不过你不要怕过期,因为我给的解决方案肯定是不会变砖的.
## 2.root权限
g300是安卓4.4.2的系统,安卓对于系统底层修改是要root权限的,而现在最流行的root就是magisk了,这里我们用最后一个支持安卓5.0以下的版本[magiskv20](https://github.com/topjohnwu/Magisk/releases/tag/v22.0),而magisk有两种获取root的方法,第一种是修改boot.img,但是我没有解包提取不了(后期等我会提取了再补,先挖个坑,),我们用到第二种方法root权限转移,先下载kingroot,这里我先用v99版本获取重启2次失败后,再安装v510版本,你会发现root已经获取到了.接下来就是magisk直接安装就好了,安装完成后会提示重启,重启后不要给kingroot权限,不然magisk会报错.
